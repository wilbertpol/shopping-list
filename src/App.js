import React, { Component } from 'react';
//import logo from './logo.svg';
import logo from './shopping-cart.svg';
import './App.css';
import uuidv4 from 'uuid/v4';

const ITEMS_KEY = 'items';
const FILTER_ALL = 'ALL';
const FILTER_BOUGHT = 'BOUGHT';
const FILTER_TO_BUY = 'TO_BUY';
const filters = {
  "ALL": {
    id: FILTER_ALL,
    method: (item) => { return true; }
  },
  "BOUGHT": {
    id: FILTER_BOUGHT,
    method: (item) => { return item.bought; }
  },
  "TO_BUY": {
    id: FILTER_TO_BUY,
    method: (item) => { return !item.bought; }
  },
}

class App extends Component {

  state = {
    items: [],
    loading: true,
    newEntry: '',
    filter: filters[FILTER_ALL],
  }

  componentDidMount() {
    const localData = localStorage.getItem(ITEMS_KEY);
    const newItems = JSON.parse(localData) || [];
    this.setState({ items: newItems, loading: false });
  }

  storeItems = (items) => {
    localStorage.setItem(ITEMS_KEY, JSON.stringify(items));
  }

  addItem = (e) => {
    e.preventDefault();

    var newItems = this.state.items.slice();
    newItems.push({
      id: uuidv4(),
      item: this.state.newEntry,
      bought: false,
    });
    this.storeItems(newItems);
    this.setState({ items: newItems, newEntry: '' });
  }

  deleteItem = (itemId) => {
    // TODO: Add confirmation for deletion
    const newItems = this.state.items.filter(item => {
      return (item.id !== itemId)
    });
    this.storeItems(newItems);
    this.setState({ items: newItems });
  }

  deleteAll = () => {
    // TODO: Add confirmation for deletion
    const itemsToRemove = this.state.items.filter((item) => this.state.filter.method(item)
    ).map((item) => { return item.id} );
    console.log('itemsToRemove', itemsToRemove);
  }

  toggleBought = (itemId) => {
    const newItems = this.state.items.map(item => {
      if (item.id === itemId) {
        return {
          ...item,
          bought: !item.bought,
        }
      }
      return item;
    })
    this.storeItems(newItems);
    this.setState({ items: newItems });
  }

  setFilter = (filterName) => {
    this.setState({filter: filters[filterName]})
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-light bg-light">
          <span className="navbar-brand mb-0 h1">
            <img src={logo} className="App-logo" alt="logo" />
            Simple Shopping List
          </span>
        </nav>

        <div className="px-3 py-2">

          <form className="form-inline my-3" onSubmit={this.addItem}>
            <div className="form-group mb-2 p-0 pr-3 col-8 col-sm-10">
              <input 
                className="form-control col-12"
                placeholder="What do you need to buy?"
                value={this.state.newEntry}
                onChange={e => this.setState({ 
                  newEntry: e.target.value 
                })}
              />
            </div>
            <button 
              type="submit" 
              className="btn btn-primary mb-2 col-4 col-sm-2">
              Add
            </button>
          </form>

          { this.state.loading && <p>Loading...</p> }

          { 
            !this.state.loading && this.state.items.length === 0 && 
            <div className="alert alert-secondary">
              Your shopping list is empty
            </div>
          }

          {
            !this.state.loading && this.state.items && this.state.items.length > 0 &&
            <div>
              <button
                onClick={() => this.setFilter(FILTER_ALL)}
                disabled={this.state.filter.id === FILTER_ALL}
              >All</button>
              &nbsp;
              <button
                onClick={() => this.setFilter(FILTER_TO_BUY)}
                disabled={this.state.filter.id === FILTER_TO_BUY}
              >To buy</button>
              &nbsp;
              <button
                onClick={() => this.setFilter(FILTER_BOUGHT)}
                disabled={this.state.filter.id === FILTER_BOUGHT}
              >Bought</button>
            </div>
          }

          {
            !this.state.loading && this.state.items && 
            <table className="table table-striped">
              <tbody>
                {
                  this.state.items.filter((item) => this.state.filter.method(item)).map((item, i) => {
                    return (
                      <tr key={item.id} className="row">
                        <td className="col-1">{i+1}</td>
                        <td className="col-9">{item.item}</td>
                        <td className="col-1">
                          <input
                            type="checkbox"
                            checked={item.bought}
                            onChange={() => this.toggleBought(item.id)}
                          ></input>
                        </td>
                        <td className="col-1">
                          <button 
                            type="button" 
                            className="close" 
                            aria-label="Close"
                            onClick={() => this.deleteItem(item.id)}
                          >
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          }

        </div>

      </div>
    );
  }
}

export default App;