# About

Simple shopping list is a simple react progressive web app for creating and managing a (grocery) shopping list. The shopping list is stored in the browser's local storage and can be used when offline.

It is based on an egghead course about creating progressive web apps. The course can be found at https://egghead.io/courses/progressive-web-apps-in-react-with-create-react-app
The repository for the course is hosted on github at https://github.com/nanohop/todo-pwa


# Building and running the application

To build and run the application locally do:
```
$ yarn build
$ serve -s build
```

